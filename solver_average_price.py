from utils import *
from pyscipopt import Model, quicksum, multidict
import itertools
from operator import itemgetter


def mtz_strong(n, c):
    """mtz_strong: Miller-Tucker-Zemlin's model for the (asymmetric) traveling salesman problem
    (potential formulation, adding stronger constraints)
    Parameters:
        n - number of nodes
        c[i,j] - cost for traversing arc (i,j)
    Returns a model, ready to be solved.
    """

    model = Model("atsp - mtz-strong")

    x, u = {}, {}
    for i in range(0, n):
        u[i] = model.addVar(lb=0, ub=n - 1, vtype="C", name="u(%s)" % i)
        for j in range(0, n):
            if i != j:
                x[i, j] = model.addVar(vtype="B", name="x(%s,%s)" % (i, j))

    for i in range(0, n):
        model.addCons(quicksum(x[i, j] for j in range(0, n) if j != i) == 1, "Out(%s)" % i)
        model.addCons(quicksum(x[j, i] for j in range(0, n) if j != i) == 1, "In(%s)" % i)

    for i in range(0, n):
        for j in range(1, n):
            if i != j:
                model.addCons(u[i] - u[j] + (n - 1) * x[i, j] + (n - 3) * x[j, i] <= n - 2, "LiftedMTZ(%s,%s)" % (i, j))

    for i in range(1, n):
        model.addCons(-x[0, i] - u[i] + (n - 3) * x[i, 0] <= -2, name="LiftedLB(%s)" % i)
        model.addCons(-x[i,0] + u[i] + (n - 3) * x[0, i] <= n - 2, name="LiftedUB(%s)" % i)

    model.setObjective(quicksum(c[i, j] * x[i, j] for (i, j) in x), "minimize")

    model.data = x, u
    return model

def sequence(arcs):
    """sequence: make a list of cities to visit, from set of arcs"""
    succ = {}
    for (i,j) in arcs:
        succ[i] = j
    curr = starting_city    # first node being visited
    sol = [curr]
    for i in range(len(arcs)):
        curr = succ[curr]
        sol.append(curr)

    return sol

weights = {}
for i in range(num_cities):
    for j in range(num_cities):
        if i != j:
            weights[i,j] = average_price(i, j)

model = mtz_strong(num_cities, weights)
model.setRealParam("limits/time", 40)
#model.setBoolParam("lp/presolving", False)
#model.setIntParam("presolving/maxrounds", 1)
#model.hideOutput() # silent mode
model.optimize()

x, u = model.data
edges = [(i,j) for (i,j) in x if model.getVal(x[i,j]) > 0.5]
sol = sequence(edges)
print(sol)
print_path(sol)

#cost = model.getSolObjVal(best_sol)
#print("Optimal value:",cost)
model.printBestSol()