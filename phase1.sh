#!/bin/bash

to_run="$@" # For now take run scripts given as arguments
timeout=20 # Timeout for a~single job in seconds


parallel -u --timeout $timeout python {} ::: $to_run
