import sys
import os
from subprocess import call

number_of_processes = int(sys.argv[1])
segment_length = int(sys.argv[2])
if len(sys.argv) >= 4:
    time_given = int(sys.argv[3])
else:
    time_given = 1000

with open('parallel_bash.sh', 'w') as f:
    f.write("#!/bin/bash\n")
    in0 = "python parallel_collector.py {} {} {}&\n".format(number_of_processes, segment_length, time_given)
    f.write(in0)
    in1 = "parallel -j0 python ::: parallel_worker.py ::: {{0..{}}}".format(number_of_processes-1)
    #in1 = "parallel --colsep , python::: parallel_collector.py,{}".format(number_of_processes)
    f.write(in1)
    #for i in range(number_of_processes):
    #    f.write(" parallel_worker.py,{}".format(i))


os.chmod('parallel_bash.sh', 0777)

rc = call("./parallel_bash.sh")
