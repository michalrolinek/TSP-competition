from utils import *    #  read variables

#Uncomment to see more functionality

#print city_list[starting_city], num_cities, city_list

#str_to_print = 'Flight from {} to {} on day {} costs {}'
#print str_to_print.format(city_list[0], city_list[1], 2, find_price(city_in=0, city_out=1, day=2))


def random_path():
    random_perm = list(np.random.permutation(num_cities))
    random_perm.remove(starting_city)
    return [starting_city] + random_perm + [starting_city]

best_score = 1e9
best_path = []
for i in range(20000):
    new_path = random_path()
    score = eval_path(new_path)
    if score is False:
        continue
    if score < best_score:
        best_score = score
        best_path = new_path

print_path(best_path)