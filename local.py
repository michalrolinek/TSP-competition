# Takes the cheepest flight possible from the city it's currently in.
# If fails picks a random path.
from utils import *    #  read variables
import random

def find_cheepest( from_city , day, not_visited ):
    best_price = False
    for to_city in not_visited:
        price = find_price( from_city, to_city, day )
        if price is not False and (price < best_price or best_price is False):
            best_price = price
            best_city = to_city
    if best_price is not False:
        return best_city
    else:
        return False

def cheepest_path():
    not_visited = range( 0, num_cities )
    current_city = starting_city
    not_visited.remove( current_city )
    path = [starting_city]
    for day in range( 0, num_cities - 1 ):
        current_city = find_cheepest( current_city, day, not_visited )
        if current_city is False:
            return False
        path.append( current_city )
        not_visited.remove( current_city )
    path.append( starting_city )
    return path

def random_path():
    random_perm = list(np.random.permutation(num_cities))
    random_perm.remove(starting_city)
    return [starting_city] + random_perm + [starting_city]

path = cheepest_path()
score = path
while score is False:
    path = random_path()
    score = eval_path( path ) 

save_path( 'current_sol', path )

#print "Local search finished with val={}".format(eval_path(path))
