from utils import *
from pyscipopt import Model, Heur, quicksum, multidict, SCIP_RESULT, SCIP_PARAMSETTING, SCIP_HEURTIMING
import itertools
import random
from operator import itemgetter
from solve_part import *

path = load_path('current_sol')

local_size = 11
while time_left() > 3.5:
    start, = random.sample(range(num_cities - local_size+2), 1)
    end = start + local_size
    path[start:end] = local_optimum(time_range=range(start, end-1), cities=path[start:end])

#print "Local solver computation (non-parallel) finished with val={}".format(eval_path(path))
save_path('current_sol', path)
