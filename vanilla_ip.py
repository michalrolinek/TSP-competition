from utils import *
from pyscipopt import Model, Heur, quicksum, multidict, SCIP_RESULT, SCIP_PARAMSETTING, SCIP_HEURTIMING
import itertools
import random
from operator import itemgetter

INFTY = 1e6

class MyHeur(Heur):

    def heurexec(self, heurtiming, nodeinfeasible):

        sol = self.model.createSol(self)
        vars = self.model.getVars()

        sol_path = get_good_path()

        for item in vars:
            i,j,t = map(int, (str(item.name)[2:-1]).split(','))
            if sol_path[t] == i and sol_path[t + 1] == j:
                value = 1
            else:
                value = 0
            self.model.setSolVal(sol, item, value)

        accepted = self.model.trySol(sol)

        if accepted:
            return {"result": SCIP_RESULT.FOUNDSOL}
        else:
            return {"result": SCIP_RESULT.DIDNOTFIND}


def build_model():

    model = Model("tdtsp - plain")
    n = num_cities
    x = {}
    for i in range(0, n):
        for j in range(0, n):
            for t in range(0, n):
                price = find_price(i, j, t)
                if ((t == 0 and i != starting_city) or
                    (t == n - 1 and j != starting_city) or
                    price is False):
                    price = INFTY

                x[i, j, t] = model.addVar(vtype="B", name="x(%s,%s,%s)" % (i, j, t), obj=price)

    for j in range(0, n):
        model.addCons(quicksum(x[i, j, t] for (i, t) in itertools.product(range(0, n), range(0, n))
                               if (i, j, t) in x) == 1, "Total_inflow(%s)" % j)

    for j in range(0, n):
        for t in range(0, n-1):
            model.addCons(quicksum(x[i, j, t] for i in range(0, n) if (i, j, t) in x) ==
                          quicksum(x[j, k, t+1] for k in range(0, n) if (j, k, t+1) in x),
                          "Flow(%s, time %s)" % (j, t))



    #model.setObjective(quicksum(find_price(i, j, t) * x[i, j, t] for (i, j, t) in x), "minimize")
    model.data = x
    #print len(x)
    return model


tsp_model = build_model()


tsp_model.hideOutput() # silent mode
tsp_model.setRealParam("limits/time", 25)
#heuristic = MyHeur()
#tsp_model.includeHeur(heuristic, "PyHeur", "custom heuristic implemented in python", "Y", timingmask=SCIP_HEURTIMING.BEFORENODE)
tsp_model.setPresolve(SCIP_PARAMSETTING.OFF)
tsp_model.optimize()
x = tsp_model.data
sol_before = [(i,t) for (i,j,t) in x if tsp_model.getVal(x[i,j,t]) > 0.5]
sol = [i for (i, t) in sorted(sol_before, key=itemgetter(1))]
sol.append(sol[0])

save_path('current_sol', sol)
#print "ILP solver finished with val={}".format(eval_path(sol))
# cost = model.getSolObjVal(best_sol)
# print("Optimal value:",cost)
#tsp_model.printBestSol()
