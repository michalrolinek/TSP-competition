#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char *argv[])
{
	ifstream compressed_data("compressed300", ios::in | ofstream::binary);

	do{
		unsigned short id_source,id_dest,day,price;
		compressed_data.read((char*)&id_source, sizeof(unsigned short));
		compressed_data.read((char*)&id_dest, sizeof(unsigned short));
		compressed_data.read((char*)&day, sizeof(unsigned short));
		compressed_data.read((char*)&price, sizeof(unsigned short));
		//cout << id_source << " " << id_dest << " " << day << " " << price << endl;
	} while(compressed_data.eof() == false);

	compressed_data.close();
	return 0;
}
