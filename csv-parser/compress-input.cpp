#include "csv.h"
#include <iostream>
#include <fstream>
#include <map>

using namespace std;
using namespace io;


inline unsigned int triple_to_index(unsigned int city_in, unsigned int city_out, unsigned int day, unsigned int dimension) {
	return day + dimension * (city_out + dimension * city_in);
}

int main(int argc, char *argv[])
{
	if(argc < 3) {
		cout << "Usage: " << argv[0] << " <path-to-input> <path-to-output>\n";
		return -1;
	}
	ofstream compressed_data(argv[2], ios::out | ofstream::binary);
	
	ifstream fin("cities", ios::in);
	string starting_city;
	fin >> starting_city;
	
	vector<string> city_list;
	while (fin) {
		string tmp;
		fin >> tmp;
		if (!tmp.empty())
			city_list.push_back(tmp);
	}
	
	unsigned int total_dimension = city_list.size() * city_list.size() * city_list.size();
	
	vector<unsigned short> data(total_dimension, 0);


	CSVReader<4, trim_chars<' '>, no_quote_escape<' '> > in(argv[1]);
	in.next_line();
	
	unsigned short start_id = distance(city_list.begin(), lower_bound(city_list.begin(), city_list.end(), starting_city));
	/*
	cerr << start_id << endl;
	cerr << city_list.size() << endl;
	for (auto city : city_list)
		cerr << city << endl;
	*/
	
	compressed_data.write((char*)&start_id, sizeof(unsigned short));

	string source, dest;
	unsigned short day, price;
	while(in.read_row(source, dest, day, price)) {
		unsigned short id_source, id_dest;
		id_source = distance(city_list.begin(), lower_bound(city_list.begin(), city_list.end(), source));
		id_dest = distance(city_list.begin(), lower_bound(city_list.begin(), city_list.end(), dest));
		data[triple_to_index(id_source, id_dest, day, city_list.size())] = price;		
	}
	
	compressed_data.write((char*)&data[0], data.size()*sizeof(unsigned short));
	compressed_data.close();
/*
	for(auto elem : dictionary_cities){
		cout << elem.first << " " << elem.second << endl;
	}
	*/ 
	return 0;
}
