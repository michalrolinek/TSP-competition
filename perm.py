# Starts in some path and then tries to switch any pair of cities.
# Stabilizes in such `local' minimum

from utils import *    #  read variables

def random_path():
    random_perm = list(np.random.permutation(num_cities))
    random_perm.remove(starting_city)
    return [starting_city] + random_perm + [starting_city]


def switch( path, a, b ):
    new_path = path[:]
    new_path[a], new_path[b] = path[b], path[a]
    return new_path

transpositions = []
for a in range( 1, num_cities - 1 ):
    for b in range( 1, num_cities - 1 ):
        if not a == b:
            transpositions.append( [ a, b ] )

path = random_path()

cur_score = eval_path( path )
score = False
while score is False or cur_score < score:
    score = cur_score
    for a,b in transpositions:
        new_path = switch( path, a, b )
        new_score = eval_path( new_path )
        if new_score is False:
            continue
        if new_score < cur_score:
            cur_score = new_score
            path = new_path

print_path( path )
