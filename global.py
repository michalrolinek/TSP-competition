# Picks the most cheep flight and then tries to connect them to a path.
# Might fail terribly!!!
from utils import *    #  read variables

not_visited = range( 0, num_cities )
path = [ -1 for i in range ( 0, num_cities + 1 ) ]

consistent_flights = [ ]
for day in range( 0, num_cities ):
    for city_from in range( 0, num_cities ):
        for city_to in range( 0, num_cities ):
            if not city_from == city_to:
                consistent_flights.append( [ city_from, city_to, day ] )

def is_consistent( flight ):
    fr, to, day = flight
    if not ((path [ day ] in [ fr, -1 ]) and 
        (path [ day+1 ] in [ to, -1 ])):
        return False
    if fr in not_visited or fr == path[ day ]:
        if to in not_visited or to == path[ day + 1 ]:
            return True
    return False

def add_city ( city, day ):
    global path, consistent_flights, not_visited
    path[ day ] = city
    try:
        not_visited.remove( city )
    except ValueError:
        pass
    new_flights = []
    for flight in consistent_flights:
        if is_consistent( flight ):
            new_flights.append( flight )
    consistent_flights = new_flights

add_city( starting_city, 0 )
add_city( starting_city, num_cities )

def add_flight ( flight ):
    global path, consistent_flights, not_visited
    fr, to, day = flight
    path[ day ] = fr
    path[ day + 1 ] = to
    try:
        not_visited.remove( fr )
    except ValueError:
        pass
    try:
        not_visited.remove( to )
    except ValueError:
        pass
    new_flights = []
    for new_flight in consistent_flights:
        if is_consistent( new_flight ) and not (flight == new_flight):
            new_flights.append( new_flight )
    consistent_flights = new_flights

def find_cheepest_flight ():
    best_price = 1e9
    for flight in consistent_flights:
        price = find_price ( *flight )
        if price > 0 and price < best_price:
            best_price = price
            best_flight = flight
    return best_flight

while -1 in path:
    next_flight = find_cheepest_flight()
    add_flight( next_flight )

print_path( path )

