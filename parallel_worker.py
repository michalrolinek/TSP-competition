from utils import *
from pyscipopt import Model, Heur, quicksum, multidict, SCIP_RESULT, SCIP_PARAMSETTING, SCIP_HEURTIMING
import itertools
import random
from operator import itemgetter
import sys
import time
import os
from solve_part import *


my_id = int(sys.argv[1])
#print 'Process with id ', my_id, " started"

def load_instructions():
    filename = 'instruction/instr{}'.format(my_id)
    if not os.path.isfile(filename):
        return False
    instructions = cPickle.load(open(filename, 'rb'))
    os.remove(filename)
    return instructions


def save_solution(sol):
    filename = 'instruction/solution{}'.format(my_id)
    return cPickle.dump(sol, open(filename, 'wb+'))

while 25.0 > time_left() > 3.2:
#for i in range(5):
    instructions = load_instructions()

    while instructions is False:
        time.sleep(0.3)
        instructions = load_instructions()
        if not (25.0 > time_left() > 2.0):
            exit()

    if instructions == 0:
        with open("die{}".format(my_id), "w") as f:
            f.write("Dead")
        exit()
    start, end = instructions
    current_path = load_path('current_sol')
    my_sol = local_optimum(time_range=range(start, end-1), cities=current_path[start:end])
    #print my_sol
    save_solution(my_sol)
    #print 'Process ', my_id, 'submitted solution'