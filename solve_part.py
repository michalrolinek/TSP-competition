from utils import *
from pyscipopt import Model, Heur, quicksum, multidict, SCIP_RESULT, SCIP_PARAMSETTING, SCIP_HEURTIMING
import itertools
import random
from operator import itemgetter

INFTY = 1e6

def local_optimum(time_range, cities):

    #print time_range, cities

    model = Model("solver - run{}".format(random.sample(range(int(1e5)),1)[0]))
    n = num_cities
    x = {}
    for i in cities[:-1]:
        for j in cities[1:]:
            for t in time_range:
                price = find_price(i, j, t)
                if ((t == time_range[0] and i != cities[0]) or
                    (t == time_range[-1] and j != cities[-1]) or
                    price is False):
                    price = INFTY


                x[i, j, t] = model.addVar(vtype="B", name="x(%s,%s,%s)" % (i, j, t), obj=price)

    for j in cities[1:]:
        model.addCons(quicksum(x[i, j, t] for (i, t) in itertools.product(cities, time_range)
                               if (i, j, t) in x) == 1, "Total_inflow(%s)" % j)

    for j in cities[1:]:
        for t in time_range[:-1]:
            model.addCons(quicksum([x[i, j, t] for i in cities if (i, j, t) in x] +
                          [-x[j, k, t+1] for k in cities if (j, k, t+1) in x]) == 0,
                          "Flow(%s, time %s)" % (j, t))

    model.data = x

    model.hideOutput() # silent mode
    model.setRealParam("limits/time", 2.8)
    model.setPresolve(SCIP_PARAMSETTING.OFF)
    model.optimize()
    x = model.data
    model.printBestSol()
    sol_before = [(i,t) for (i,j,t) in x if model.getVal(x[i,j,t]) > 0.5]
    sol = [i for (i, t) in sorted(sol_before, key=itemgetter(1))]
    sol.append(cities[-1])
    model.freeProb()
    del model
    #print sol
    return sol

'''

'''