# Takes the cheepest flight possible from the city it's currently in.
# After that tries to switch random two cities for a few times.
#
# May result in a terrible crash if it gets stuck in a city with no flight
# from.
from utils import *    #  read variables
import random

not_visited = range( 0, num_cities )
current_city = starting_city
not_visited.remove( current_city )
path = [starting_city]

def find_cheepest( from_city , day ):
    best_price = 1e9
    for to_city in not_visited:
        price = find_price( from_city, to_city, day )
        if price > 0 and price < best_price:
            best_price = price
            best_city = to_city
    return best_city

for day in range( 0, num_cities - 1 ):
    current_city = find_cheepest( current_city, day )
    path.append( current_city )
    not_visited.remove( current_city )
path.append( starting_city )

def switch( path, a, b ):
    new_path = path[:]
    new_path[a], new_path[b] = path[b], path[a]
    return new_path

#transpositions = []
#for a in range( 1, num_cities - 1 ):
#    for b in range( 1, num_cities - 1 ):
#        if not a == b:
#            transpositions.append( [ a, b ] )
#
#cur_score = eval_path( path )
#score = False
#n = 0
#while score is False or cur_score < score:
#    score = cur_score
#    for a,b in transpositions:
#        n += 1
#        new_path = switch( path, a, b )
#        new_score = eval_path( new_path )
#        if new_score is False:
#            continue
#        if new_score < cur_score:
#            cur_score = new_score
#            path = new_path

cur_score = eval_path( path )
for i in range(20000):
    score = cur_score
    a,b = random.sample( range( 1, num_cities -1 ), 2 )
    new_path = switch( path, a, b )
    new_score = eval_path( new_path )
    if new_score is False:
        continue
    if new_score < cur_score:
        cur_score = new_score
        path = new_path
        print_path( path )
        print i, cur_score

