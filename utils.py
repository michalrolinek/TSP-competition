import numpy as np

import random
import cPickle

import time

with open("compressed_input", "r") as f:
    data = np.fromfile(f, dtype=np.uint16)

with open("cities", "r") as f:
    content = f.readlines()

with open("start_time", "r") as f:
    time_data = f.readlines()

start_time = float( time_data[0] )


city_list = [item.rstrip() for item in content[1:]]
num_cities = len(city_list)
starting_city = data[0]

def save_path(filename, path):
    cPickle.dump(path, open(filename, 'wb'))


def load_path(filename):
    return cPickle.load(open(filename, 'rb'))


def average_price(city_in, city_out):
    values = [find_price(city_in, city_out, day) if find_price(city_in, city_out, day) is not False else 10000 for day in range(num_cities)]
    return sum(values) / len(values)

def find_price(city_in, city_out, day):
    price = data[1 + day + (num_cities * (city_out + num_cities * city_in))]
    if price != 0:
        return price
    else:
        #print "No edge: ", city_in, city_out, day
        return False


def price_string(city_in, city_out, day):
    price = find_price(city_in, city_out, day)
    if price is not False:
        return '{} {} {} {}'.format(city_list[city_in], city_list[city_out], day, price)
    else:
        return False


def restore_file(out_fname):
    with open(out_fname, "w") as f:
        f.write('{}\n'.format(city_list[starting_city]))
        for i in range(len(city_list)):
            for j in range(len(city_list)):
                for k in range(len(city_list)):
                    to_print = price_string(i, j, k)
                    if to_print is not False:
                        f.write(to_print)


def eval_path(path):
    if len(path) != num_cities +1:
        return False
    if path[0] != starting_city:
        return False
    if path[-1] != starting_city:
        return False
    if sorted(path[1:]) != range(num_cities):
        return False
    total_price = 0
    for day, (city_from, city_to) in enumerate(zip(path[:-1], path[1:])):
        price = find_price(city_from, city_to, day)
        if price is False:
            return False
        total_price += price
    return total_price


def path_strings(path):
    if len(path) != num_cities +1:
        return False
    if path[0] != starting_city:
        return False
    if path[-1] != starting_city:
        return False
    if sorted(path[1:]) != range(num_cities):
        return False
    result = [price_string(city_from, city_to, day) for day, (city_from, city_to) in enumerate(zip(path[:-1], path[1:]))]
    return result


def print_path(path):
    print eval_path(path)
    to_print = path_strings(path)
    for line in to_print:
        print line


def save_path_printable(filename, path):
    with open(filename, "w") as f:
        f.write('{}/\n'.format(eval_path(path)))
        to_print = path_strings(path)
        for line in to_print:
            f.write('{}/\n'.format(line))

def random_path():
    random_perm = list(np.random.permutation(num_cities))
    random_perm.remove(starting_city)
    return [starting_city] + random_perm + [starting_city]


def random_valid_path():
    new_path = random_path()
    while eval_path(new_path) is False:
        new_path = random_path()
    return new_path


def swap_two(path):
    a, b = random.sample(range(1, num_cities - 1), 2)
    new_path = path[:]
    new_path[a], new_path[b] = path[b], path[a]
    return new_path


def get_good_path():
    best_path = random_valid_path()
    best_score = eval_path(best_path)
    assert best_score is not False
    for i in range(num_cities ** 2):
        new_path = swap_two(best_path)
        score = eval_path(new_path)
        if score is False:
            continue
        if score < best_score:
            best_score = score
            best_path = new_path
    return best_path


def time_elapsed():
    return time.time() - start_time

def time_left():
    return start_time + 30 - time.time()
