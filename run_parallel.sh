#!/bin/bash
# A sketch of the whole algorithm

## READ INPUT

cat > current_input
if ! [ -s current_input ]
then
	STDIN= $(</dev/stdin)
	echo "$STDIN" > current_input
fi

## PREPROCESSING

date +"%s.%3N" > start_time
head -1 current_input > cities
head -100000 current_input | cut -d' ' -f2 | sort -u >> cities

./csv-parser/compress current_input compressed_input

## PHASE I

phase1="local.py some_other_stuff.py"
parallel -u --timeout 10 python ::: $phase1
wait
python collect_phase1.py

## PHASE II

inputs=$( python parallel_collector.py )
parallel -u --timeout 20 python parallel_worker.py ::: $inputs
wait
python parallel_collect.py
# maybe repeat?

## CLEAN UP AND OUTPUT


