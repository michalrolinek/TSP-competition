# A combination of all previous things:
#
# 1/ Randomly inverts part of the path, or
# 2/ randomly switches two cities
# */ Repeats 1/ & 2/ for some time
#
# Can run on 300 flights in reasonable time (~47 seconds on my computer)
#
from utils import *    #  read variables
import random
import sys

allowed_time = float( sys.argv[1] )
end_time = time.time() + allowed_time

def time_to_stop( epsilon ):
    remaining = end_time - time.time()
    if remaining < epsilon:
        return True
    return False

def invert( path, a, b ):
    if b>a:
        new_path = path[:]
        new_path[a:b+1] = path[b:a-1:-1]
        return new_path
    if a>b:
        new_path = path[:]
        new_path[b:a+1] = path[a:b-1:-1]
        return new_path
    return path

def permute( path, a, b, c ):
    new_path = path[:]
    new_path[a], new_path[b], new_path[c] = path[b], path[c], path[a]
    return new_path

def eval_local(path, a, b):
    if a>b:
        a,b = b,a
    total_price = 0
    for day, city_from, city_to in zip(
            range(a-1,b+1), path[a-1:b+1], path[a:b+2]):
        price = find_price(city_from, city_to, day)
        if price is False:
            return False
        total_price += price
    return total_price

def random_transpositions( path, n ):
    for i in range(n):
        choose = random.choice ( [True, False] )
        if choose:
            a,b = random.sample( range( 1, num_cities - 1 ), 2 )
            new_path = invert( path, a, b )
            change = eval_local( new_path, a, b )
            if change is not False:
                change -= eval_local( path, a, b )
        else:
            a,b,c = random.sample( range( 1, num_cities - 1 ), 3 )
            new_path = permute( path, a, b, c )
            m = max( [a,b,c] )
            n = min( [a,b,c] )
            change = eval_local( new_path, n, m )
            if change is not False:
                change -= eval_local( path, n, m )
        if change is not False and change <= 0:
            #print change,eval_path( new_path )
            path = new_path
    return path

path = load_path( 'current_sol' )

path = random_transpositions( path, 10000 )

while not time_to_stop( .2 ):
    path = random_transpositions( path, 300 )

save_path( 'current_sol', path )
#print "Random mutations finished with val={}".format(eval_path(path))
