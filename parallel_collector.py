from utils import *

import time
import os.path
import random
import sys


path = load_path('current_sol')
best_score = eval_path(path)
save_path("current_sol", path)
local_len = int(sys.argv[2])
process_num = int(sys.argv[1])
time_given = int(sys.argv[3])
iterations = 5

end_time = time.time() + time_given

def time_to_stop( epsilon ):
    remaining = end_time - time.time()
    if remaining < epsilon:
        return True
    return False

def save_instructions(index, instruct):
    filename = 'instruction/instr{}'.format(index)
    with open(filename, 'wb+') as f:
        cPickle.dump(instruct, f)

def load_solution(index):
    filename = 'instruction/solution{}'.format(index)
    with open(filename, 'rb') as f:
        return cPickle.load(f)
    return None

def all_sols_ready():
    for i in range(process_num):
        filename = 'instruction/solution{}'.format(i)
        if not os.path.isfile(filename):
            return False
    return True

def clean_sol_files():
    for i in range(process_num):
        filename = 'instruction/solution{}'.format(i)
        os.remove(filename)





def prepare_instructions():
    avg_slack = ((num_cities - process_num*local_len) / process_num)
    arr_1 = range(0, num_cities - local_len - avg_slack+1, local_len + avg_slack)
    rand_nums = [random.randint(0, avg_slack) for p in range(len(arr_1))]
    arr_1 = [a + b for (a, b) in zip(arr_1, rand_nums)]
    arr_2 = [item + local_len for item in arr_1]
    assert len(arr_1) == len(arr_2)
    instructions = zip(arr_1, arr_2)
    instructions = random.sample(instructions, process_num)
    #print instructions
    return instructions


while 25.0 > time_left() > 3.0 and not time_to_stop(1.0):
#for iter_num in range(iterations):

    instruction_set = prepare_instructions()
    for i, instruct in enumerate(instruction_set):
        save_instructions(i, instruct)

    #print 'Instructions sent'
    while not all_sols_ready():
        time.sleep(0.3)

    #print 'Solutions ready'
    for i, instruct in enumerate(instruction_set):
        sol = load_solution(i)
        if sol:
            new_path = path[:]
            new_path[instruct[0]:instruct[1]] = sol
            new_score = eval_path(new_path)
            if new_score is False or new_score > best_score:
                #print 'Shit!'
                pass
            if new_score is not False and new_score < best_score:
                #print 'Path improved!'
                best_score = new_score
                path = new_path
    #print 'Solutions updated'
    #print eval_path(path)
    save_path("current_sol", path)

    clean_sol_files()

instruction_set = [0] * process_num
for i, instruct in enumerate(instruction_set):
    save_instructions(i, instruct)
    #print "Files cleaned"

#print "Parallel computation finished with val={}".format(eval_path(path))
save_path('current_sol', path)
#print_path(path)
